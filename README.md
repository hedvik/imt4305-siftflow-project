# IMT4305 - SiftFlow Project #
This project is focused on modifying the input data to the SiftFlow algorithm to see if there are any large differences between accuracy and performance between various image types. 
The primary Matlab code to look on is demo.m as this is where input to the algorithm is handled. 

* There are 2 major branches:
	* Initial tests related to Angular Error are found on the master branch
	* Frame interpolation tests are found on the VideoFrameInterpolation branch
		* The video clips used for testing are from the Big Buck Bunny short movie which is attributed under a Creative Commons 3.0 license.

* Test results spreadsheet can be found here:
	* https://docs.google.com/spreadsheets/d/1wbJa6NHzks0M0rTEfTdkadLnPu8gtwgTACKrrf9Sass/edit?usp=sharing

* Presentation slides can be found here:
	* https://docs.google.com/presentation/d/1puB_11yJ5TaM6CV1SDO_0YvUjETC1SojKtGEyIgAwwg/edit#slide=id.p
	
