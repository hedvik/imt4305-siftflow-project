function [im1, im2, im1Gray, im2Gray, im1GrayGradient, im2GrayGradient, im1Sift, im2Sift] = readAndPrepareTestCaseImages(im1Path, im2Path, gradientMultiplier, cellsize, gridspacing)
im1 = readImageFiltered(im1Path);
im2 = readImageFiltered(im2Path);

im1Gray = rgb2gray(im1);
im2Gray = rgb2gray(im2);
im1GrayGradient = addGradient(im1Gray, gradientMultiplier);
im2GrayGradient = addGradient(im2Gray, gradientMultiplier);

im1Sift = mexDenseSIFT(im1,cellsize,gridspacing);
im2Sift = mexDenseSIFT(im2,cellsize,gridspacing);
end

