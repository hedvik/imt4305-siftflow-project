cellsize=3;
gridspacing=1;

addpath(fullfile(pwd,'mexDenseSIFT'));
addpath(fullfile(pwd,'mexDiscreteFlow'));
addpath(fullfile(pwd,'ground_truth_data'));

% Load all the images
gradientMultiplier = 0.75;

[rubberWhale1, rubberWhale2, ... 
    rubberWhale1Gray, rubberWhale2Gray, ...
    rubberWhale1GrayGradient, rubberWhale2GrayGradient, ...
    rubberWhale1Sift, rubberWhale2Sift] ...
    = readAndPrepareTestCaseImages('./ground_truth_data/other-data/RubberWhale/frame10.png', ...
                                   './ground_truth_data/other-data/RubberWhale/frame11.png', ...
                                   gradientMultiplier, cellsize, gridspacing);
%%%%%%%%%%%
[dimetrodon1, dimetrodon2, ... 
    dimetrodon1Gray, dimetrodon2Gray, ...
    dimetrodon1GrayGradient, dimetrodon2GrayGradient, ...
    dimetrodon1Sift, dimetrodon2Sift] ...
    = readAndPrepareTestCaseImages('./ground_truth_data/other-data/Dimetrodon/frame10.png', ...
                                   './ground_truth_data/other-data/Dimetrodon/frame11.png', ...
                                   gradientMultiplier, cellsize, gridspacing);
%%%%%%%%%%%%
[venus1, venus2, ... 
    venus1Gray, venus2Gray, ...
    venus1GrayGradient, venus2GrayGradient, ...
    venus1Sift, venus2Sift] ...
    = readAndPrepareTestCaseImages('./ground_truth_data/other-data/Venus/frame10.png', ...
                                   './ground_truth_data/other-data/Venus/frame11.png', ...
                                   gradientMultiplier, cellsize, gridspacing);
%%%%%%%%%%%%
[hydrangea1, hydrangea2, ... 
    hydrangea1Gray, hydrangea2Gray, ...
    hydrangea1GrayGradient, hydrangea2GrayGradient, ...
    hydrangea1Sift, hydrangea2Sift] ...
    = readAndPrepareTestCaseImages('./ground_truth_data/other-data/Hydrangea/frame10.png', ...
                                   './ground_truth_data/other-data/Hydrangea/frame11.png', ...
                                   gradientMultiplier, cellsize, gridspacing);
% Load ground truth
rubberWhaleGT = readFlowFiltered('./ground_truth_data/other-gt-flow/RubberWhale/flow10.flo');
dimetrodonGT = readFlowFiltered('./ground_truth_data/other-gt-flow/Dimetrodon/flow10.flo');
venusGT = readFlowFiltered('./ground_truth_data/other-gt-flow/Venus/flow10.flo');
hydrangeaGT = readFlowFiltered('./ground_truth_data/other-gt-flow/Hydrangea/flow10.flo');

% Place images in respective cell arrays
im1Array = {rubberWhale1Sift, rubberWhale1, rubberWhale1Gray, rubberWhale1GrayGradient};
im1Array = [im1Array; {dimetrodon1Sift, dimetrodon1, dimetrodon1Gray, dimetrodon1GrayGradient}];
im1Array = [im1Array; {venus1Sift, venus1, venus1Gray, venus1GrayGradient}];
im1Array = [im1Array; {hydrangea1Sift, hydrangea1, hydrangea1Gray, hydrangea1GrayGradient}];

im2Array = {rubberWhale2Sift, rubberWhale2, rubberWhale2Gray, rubberWhale2GrayGradient};
im2Array = [im2Array; {dimetrodon2Sift, dimetrodon2, dimetrodon2Gray, dimetrodon2GrayGradient}];
im2Array = [im2Array; {venus2Sift, venus2, venus2Gray, venus2GrayGradient}];
im2Array = [im2Array; {hydrangea2Sift, hydrangea2, hydrangea2Gray, hydrangea2GrayGradient}];

groundTruthArray = {rubberWhaleGT; dimetrodonGT; venusGT; hydrangeaGT};

SIFTflowpara.alpha=2*255;
SIFTflowpara.d=40*255;
SIFTflowpara.gamma=0.005*255;
SIFTflowpara.nlevels=4;
SIFTflowpara.wsize=2;
SIFTflowpara.topwsize=10;
SIFTflowpara.nTopIterations = 60;
SIFTflowpara.nIterations= 30;

% Performing optical flow tests for each method: SIFT, RGB and
% Grayscale
performanceData = {'Sift', 0, 0; 'RGB', 0, 0; 'Grayscale', 0, 0; 'GrayscaleGradient', 0, 0};
numImages = size(groundTruthArray, 1);
numFlowMethods = 4;

% For each image, then for each flow calculation
for i = 1:numImages
    for j = 1:numFlowMethods
       tic;
       [vx,vy,~]=SIFTflowc2f(im1Array{i, j}, im2Array{i, j}, SIFTflowpara);
       timeSpentCalculating = toc;

       % Get Flow
       clear flow;
       flow(:,:,1)=vx;
       flow(:,:,2)=vy;

       % Compare with ground truth
       [aae, ~] = flow_aae(flow, groundTruthArray{i});
       
       performanceData{j, 2} = performanceData{j, 2} + timeSpentCalculating;
       performanceData{j, 3} = performanceData{j, 3} + aae;
    end
end

% Make sure our sums are averaged out
for i = 1:numFlowMethods
    performanceData{i, 2} = performanceData{i, 2} / numImages;
    performanceData{i, 3} = performanceData{i, 3} / numImages;
end

% Write results to file
% 2 is apparently equivalent to true
if exist('testResults.csv', 'file') == 2
    fid = fopen('testResults.csv', 'at');
else
    fid = fopen('testResults.csv', 'w');
    fprintf(fid, '%s, %s, %s\n', 'ImageType', 'TimeSpentCalculating', 'AngularError');
end

for i = 1:numFlowMethods
    fprintf(fid, '%s, %.5f, %.5f \n', performanceData{i, 1}, performanceData{i, 2}, performanceData{i, 3});
end
fclose(fid);

return;