function [img] = readFlowFiltered(flowPath)
    img = readFlowFile(flowPath);
    img = imresize(imfilter(img,fspecial('gaussian',7,1.),'same','replicate'),0.5,'bicubic');
    img = im2double(img);
end

