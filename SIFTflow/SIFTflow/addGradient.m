function [gradientImage] = addGradient(image, multiplier)
    [gmag, ~] = imgradient(image, 'prewitt');
    gmag = gmag * multiplier;
    gradientImage = image;
    gradientImage(:, :, 2) = gmag;
end

